# just adding a comment

data "aws_ami" "public_ami" {
  most_recent = true
  owners = ["591542846629"]
  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-2.0.20190603-x86_64-ebs"]
  }
}

resource "aws_instance" "web" {
  ami = data.aws_ami.public_ami.id
  instance_type = "t2.micro"
  tags = {
    Name = "Daniela instance tf"
  }
}

